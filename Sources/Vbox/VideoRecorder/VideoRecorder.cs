using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using AForge.Video.FFMPEG;
using AForge.Video;
using System.Diagnostics;
using System.IO;

namespace Vbox
{
    /// <summary>
    /// 定义的录制视频类，里面包含了录制时需要的一些变量
    /// </summary>
    public partial class VideoRecorder : Form
    {
        private bool _isRecording; //是否录制
        private List<string> _screenNames;
        private UInt32 _frameCount;
        private VideoFileWriter _writer;
        private int _width;//宽度
        private int _height;//高度
        private ScreenCaptureStream _streamVideo;
        private Stopwatch _stopWatch;
        private Rectangle _screenArea;
        private int BitRate = 1000;
        private int Fps = 10;

        private RepositoryModel Repository;

        public VideoRecorder(RepositoryModel repository) //录制视频
        {
            InitializeComponent(); //初始化组件

            this.Repository = repository;

            this._isRecording = false;
            this._frameCount = 0;
            this._width = SystemInformation.VirtualScreen.Width;
            this._height = SystemInformation.VirtualScreen.Height;
            this._stopWatch = new Stopwatch();
            this._screenArea = Rectangle.Empty;

            //this.bt_Save.Enabled = false;
            this._writer = new VideoFileWriter();

            _screenNames = new List<string>();
            _screenNames.Add(@"Select ALL");
            foreach (var screen in Screen.AllScreens)
            {
                _screenNames.Add(screen.DeviceName);
            }
            this.cb_screenSelector.DropDownStyle = ComboBoxStyle.DropDownList;
            this.cb_screenSelector.DataSource = _screenNames;

            // Codec ComboBox
            //this.cb_VideoCodec.DataSource = Enum.GetValues(typeof(VideoCodec));
            //this.cb_VideoCodec.DropDownStyle = ComboBoxStyle.DropDownList;

            ///<summary> 
            /// 比特率 2000kbit/s 2000000 1000000
            ///</summary>
            //this.cb_BitRate.DataSource = Enum.GetValues(typeof(BitRate));
            //this.cb_BitRate.DropDownStyle = ComboBoxStyle.DropDownList;
            //this.cb_BitRate.SelectedIndex = 3;
            //panel1.Visible = false;

            //this.tb_SaveFolder.Text = open; //保存视频的路径
        }

        private void StartRec(string path) //开始录制
        {
            if (_isRecording == false)
            {
                //panel1.Visible = true;
                this.SetScreenArea();

                this.SetVisible(true);

                this._frameCount = 0;

                //this.tb_SaveFolder.Text = path;

                //视频文件名字
                string FullPath = $"{path}\\{DateTime.Now.ToString("yyyy_MM_d_HH_mm")}";
                const string Extension = ".mp4";

                while (File.Exists(FullPath + Extension))
                {
                    FullPath += $"(1)";
                }
                FullPath += Extension;

                Repository.AddFile(FullPath);

                // Save File option
                _writer.Open(
                    FullPath,
                    this._width,
                    this._height,
                    Fps,
                    VideoCodec.MPEG4,
                    //(VideoCodec)cb_VideoCodec.SelectedValue,
                    BitRate);

                // Start main work
                this.StartRecord();
            }
        }

        private void SetScreenArea() //设置录制的屏幕范围
        {
            // get entire desktop area size
            string screenName = this.cb_screenSelector.SelectedValue.ToString();
            if (string.Compare(screenName, @"Select ALL", StringComparison.OrdinalIgnoreCase) == 0) //如果是全屏
            {
                foreach (Screen screen in Screen.AllScreens)
                {

                    this._screenArea = Rectangle.Union(_screenArea, screen.Bounds);
                }
            }
            else
            {
                this._screenArea = Screen.AllScreens.First(scr => scr.DeviceName.Equals(screenName)).Bounds;
                this._width = this._screenArea.Width;
                this._height = this._screenArea.Height;
            }
        }

        private void StartRecord() //Object stateInfo
        {
            // create screen capture video source
            this._streamVideo = new ScreenCaptureStream(this._screenArea);

            // set NewFrame event handler
            this._streamVideo.NewFrame += new NewFrameEventHandler(this.video_NewFrame);

            // start the video source
            this._streamVideo.Start();

            // _stopWatch
            this._stopWatch.Start();
        }

        private void video_NewFrame(object sender, NewFrameEventArgs eventArgs)//视频窗口
        {

            if (this._isRecording)
            {
                this._frameCount++;

                this._writer.WriteVideoFrame(eventArgs.Frame); //写入视频

                //this.lb_1.Invoke(new Action(() =>
                //{
                //    lb_1.Text = string.Format(@"Frames: {0}", _frameCount);
                //}));

                this.lb_stopWatch.Invoke(new Action(() =>
                {
                    var timeSpan = _stopWatch.Elapsed;

                    this.lb_stopWatch.Text = String.Format(
                        "{0:00}:{1:00}:{2:00}.{3:00}",
                        timeSpan.Hours,
                        timeSpan.Minutes,
                        timeSpan.Seconds,
                        timeSpan.Milliseconds / 10
                    );
                }));
            }
            else
            {
                _stopWatch.Reset(); //重置
                Thread.Sleep(500);//睡眠
                _streamVideo.SignalToStop();
                Thread.Sleep(500);
                _writer.Close();//关闭写
            }
        }

        private void SetVisible(bool visible) //设置是否可见
        {
            //this.bt_start.Enabled = !visible;
            //this.bt_Save.Enabled = visible;
            this._isRecording = visible;
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            this._isRecording = false;
        }

        private void RecordButton_Click(object sender, EventArgs e)
        {
            if (!_isRecording)
            {
                try
                {
                    //if (true/*this.tb_SaveFolder.Text.Length < 1*/) //路径长度小于1
                    //{
                    FolderBrowserDialog fbd = new FolderBrowserDialog();
                    if (fbd.ShowDialog() == DialogResult.OK)
                    {
                        this.RecordButton.Image = Properties.Resources.stop;
                        this.StartRec(fbd.SelectedPath);
                    }
                    //}
                    //else
                    //{
                    //    this.StartRec(this.tb_SaveFolder.Text); //开始录制
                    //}
                    //if (tb_SaveFolder.Text.Length == 0)//路径长度等于0
                    //{
                    //    panel1.Visible = false;
                    //}
                }
                catch (Exception exc)
                {
                    MessageBox.Show(exc.Message);
                }
            }
            else
            {
                this.RecordButton.Image = Properties.Resources.record;
                //panel1.Visible = false;
                this.SetVisible(false);
                MessageBox.Show(@"File saved!");//录制完了之后，跳出文件保存的窗口
            }
        }
    }

    //public enum BitRate //屏幕的bit率（一个下拉框）
    //{
    //    _50kbit = 5000,
    //    _100kbit = 10000,
    //    _500kbit = 50000,
    //    _1000kbit = 1000000,
    //    _2000kbit = 2000000,
    //    _3000kbit = 3000000
    //}
}

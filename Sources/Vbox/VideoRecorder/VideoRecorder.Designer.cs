﻿namespace Vbox
{
    partial class VideoRecorder
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            //if (_writer.IsOpen)
            //{
            //    _streamVideo.SignalToStop();
            //    _writer.Close();
            //    _writer.Dispose();
            //}
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.cb_screenSelector = new System.Windows.Forms.ComboBox();
            this.lb_stopWatch = new System.Windows.Forms.Label();
            this.RecordButton = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.RecordButton)).BeginInit();
            this.SuspendLayout();
            // 
            // cb_screenSelector
            // 
            this.cb_screenSelector.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(75)))), ((int)(((byte)(81)))), ((int)(((byte)(105)))));
            this.cb_screenSelector.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cb_screenSelector.ForeColor = System.Drawing.SystemColors.Menu;
            this.cb_screenSelector.FormattingEnabled = true;
            this.cb_screenSelector.Location = new System.Drawing.Point(14, 83);
            this.cb_screenSelector.Name = "cb_screenSelector";
            this.cb_screenSelector.Size = new System.Drawing.Size(151, 26);
            this.cb_screenSelector.TabIndex = 89;
            // 
            // lb_stopWatch
            // 
            this.lb_stopWatch.AutoSize = true;
            this.lb_stopWatch.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold);
            this.lb_stopWatch.ForeColor = System.Drawing.Color.Gainsboro;
            this.lb_stopWatch.Location = new System.Drawing.Point(6, 22);
            this.lb_stopWatch.Name = "lb_stopWatch";
            this.lb_stopWatch.Size = new System.Drawing.Size(178, 29);
            this.lb_stopWatch.TabIndex = 15;
            this.lb_stopWatch.Text = "00:00:00.00";
            // 
            // RecordButton
            // 
            this.RecordButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.RecordButton.Image = global::Vbox.Properties.Resources.record;
            this.RecordButton.Location = new System.Drawing.Point(199, 19);
            this.RecordButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.RecordButton.Name = "RecordButton";
            this.RecordButton.Size = new System.Drawing.Size(32, 32);
            this.RecordButton.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.RecordButton.TabIndex = 97;
            this.RecordButton.TabStop = false;
            this.RecordButton.Click += new System.EventHandler(this.RecordButton_Click);
            // 
            // VideoRecorder
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(243, 71);
            this.Controls.Add(this.RecordButton);
            this.Controls.Add(this.lb_stopWatch);
            this.Controls.Add(this.cb_screenSelector);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.Name = "VideoRecorder";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.RecordButton)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.ComboBox cb_screenSelector;
        private System.Windows.Forms.Label lb_stopWatch;
        private System.Windows.Forms.PictureBox RecordButton;
    }
}


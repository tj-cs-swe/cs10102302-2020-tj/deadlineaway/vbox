﻿using AxMOVIEPLAYERLib;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Vbox
{
    public partial class VideoPlayerView : Form
    {
        private VideoPlayerModel VideoPlayer;

        public VideoPlayerView(VideoPlayerModel videoPlayer)
        {
            VideoPlayer = videoPlayer;
            InitializeComponent();

            AxVideoPlayer.FileName = VideoPlayer.File.FullPath;
            DateTime EndTime = DateTime.Parse(DateTime.Now.ToString("00:00:00")).AddSeconds(AxVideoPlayer.Duration);
            EndTimeLable.Text = String.Format("{0:HH:mm:ss}", EndTime);
            UpdateVolumeFromModel();
            UpdatePlaybackRateFromModel();
            UpdateVideoProgressFromModel();
            AxVideoPlayer.StepFrame(1);

            SpeedComboBox.SelectedIndex = 2;
            VolumeBar.Value = (int)(VolumeBar.Maximum * VideoPlayer.VolumeRate);
        }

        private void UpdateVolumeFromModel()
        {
            var Rate = VideoPlayer.VolumeRate;
            AxVideoPlayer.SoundVolume = Rate == 0 ? -10000 : -1000 + (int)(1000 * Rate);
            VolumeBar.Value = (int)(VolumeBar.Maximum * VideoPlayer.VolumeRate);
        }

        private void UpdateVideoProgressFromModel()
        {
            ProgressBar.Value = (int)(ProgressBar.Maximum * VideoPlayer.PlayProgressRate);
            AxVideoPlayer.SetPos(AxVideoPlayer.Duration * VideoPlayer.PlayProgressRate);
        }

        private void UpdatePlaybackRateFromModel()
        {
            AxVideoPlayer.PlaybackRate = VideoPlayer.PlaybackRate;
        }

        private void AxVideoPlayer_OnCompleted(object sender, EventArgs e)
        {
            VideoPlayer.Stop();
            UpdatePlayStateFromModel();
            this.DialogResult = DialogResult.Cancel;
        }

        private void ProgressBar_Scroll(object sender, EventArgs e)
        {
            VideoPlayer.PlayProgressRate = (double)(ProgressBar.Value) / (double)(ProgressBar.Maximum);
            UpdateVideoProgressFromModel();
        }

        private void VolumeBar_Scroll(object sender, EventArgs e)
        {
            VideoPlayer.VolumeRate = (double)(VolumeBar.Value) / (double)(VolumeBar.Maximum);
            UpdateVolumeFromModel();
        }

        private void SpeedComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            var Text = SpeedComboBox.SelectedItem.ToString();
            var Rate = Convert.ToDouble(Text.Substring(0, Text.Length - 1));
            VideoPlayer.PlaybackRate = Rate;
            UpdatePlaybackRateFromModel();
        }

        private void SpeedUpButton_Click(object sender, EventArgs e)
        {
            var newRate = Math.Min(1.0, VideoPlayer.PlayProgressRate + 30.0 / AxVideoPlayer.Duration);
            VideoPlayer.PlayProgressRate = newRate;
            UpdateVideoProgressFromModel();
        }

        private void SpeedDownButton_Click(object sender, EventArgs e)
        {
            var newRate = Math.Max(0.0, VideoPlayer.PlayProgressRate - 30.0 / AxVideoPlayer.Duration);
            VideoPlayer.PlayProgressRate = newRate;
            UpdateVideoProgressFromModel();
        }

        private void PlayButton_Click(object sender, EventArgs e)
        {
            switch (VideoPlayer.PlayState)
            {
                case VideoPlayerModel.State.Paused:
                case VideoPlayerModel.State.Stopped:
                    VideoPlayer.Play();
                    break;
                case VideoPlayerModel.State.Playing:
                    VideoPlayer.Pause();
                    break;
            }
            UpdatePlayStateFromModel();
        }

        private void UpdatePlayStateFromModel()
        {
            switch (VideoPlayer.PlayState)
            {
                case VideoPlayerModel.State.Playing:
                    AxVideoPlayer.Play();
                    break;
                case VideoPlayerModel.State.Stopped:
                    AxVideoPlayer.Stop();
                    break;
                case VideoPlayerModel.State.Paused:
                    AxVideoPlayer.Pause();
                    break;
            }
            PlayButton.Image = VideoPlayer.PlayState == VideoPlayerModel.State.Playing ?
                global::Vbox.Properties.Resources.stop : global::Vbox.Properties.Resources.playfill;
        }

        private void AxVideoPlayer_OnPlaying(object sender, AxMOVIEPLAYERLib._DMoviePlayerEvents_OnPlayingEvent e)
        {
            VideoPlayer.PlayProgressRate = e.iCurrent / (double) AxVideoPlayer.Duration;
            DateTime StartTime = DateTime.Parse(DateTime.Now.ToString("00:00:00")).AddSeconds(e.iCurrent);
            StartTimeLable.Text = String.Format("{0:HH:mm:ss}", StartTime);
            ProgressBar.Value = (int)(ProgressBar.Maximum * VideoPlayer.PlayProgressRate);
        }

        private void VolumeButton_Click(object sender, EventArgs e)
        {
            VolumeBar.Visible = !VolumeBar.Visible;
        }

        private void ResizeVideoPanel()
        {
            AxVideoPlayer.ResizeControl((short)this.VideoPlayAreaPanel.Width, (short)this.VideoPlayAreaPanel.Height);
        }

        private void VideoPlayAreaPanel_Resize(object sender, EventArgs e)
        {
            ResizeVideoPanel();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vbox
{
    public class VideoPlayerModel
    {
        public FileModel File { get; private set; }

        public VideoPlayerModel(FileModel file)
        {
            File = file;
        }

        public double VolumeRate = 1;

        public double PlayProgressRate = 0;

        public double PlaybackRate = 1;

        public enum State
        {
            Playing, Stopped, Paused
        }

        public State PlayState { get; private set; } = State.Stopped;

        public void Stop()
        {
            PlayState = State.Stopped;
        }

        public void Play()
        {
            PlayState = State.Playing;
        }

        public void Pause()
        {
            PlayState = State.Paused;
        }
    }
}

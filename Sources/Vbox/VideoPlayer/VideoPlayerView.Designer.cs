﻿namespace Vbox
{
    partial class VideoPlayerView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VideoPlayerView));
            this.ContainerPanel = new System.Windows.Forms.Panel();
            this.VideoPanel = new System.Windows.Forms.Panel();
            this.VolumeBar = new System.Windows.Forms.TrackBar();
            this.VideoPlayAreaPanel = new System.Windows.Forms.Panel();
            this.AxVideoPlayer = new AxMOVIEPLAYERLib.AxMoviePlayer();
            this.PlayerPanel = new System.Windows.Forms.Panel();
            this.VolumeButton = new System.Windows.Forms.PictureBox();
            this.EndTimeLable = new System.Windows.Forms.Label();
            this.StartTimeLable = new System.Windows.Forms.Label();
            this.SpeedPanel = new System.Windows.Forms.Panel();
            this.SpeedComboBox = new System.Windows.Forms.ComboBox();
            this.SpeedDownButton = new System.Windows.Forms.PictureBox();
            this.SpeedUpButton = new System.Windows.Forms.PictureBox();
            this.PlayButton = new System.Windows.Forms.PictureBox();
            this.ProgressBar = new System.Windows.Forms.TrackBar();
            this.ContainerPanel.SuspendLayout();
            this.VideoPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.VolumeBar)).BeginInit();
            this.VideoPlayAreaPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AxVideoPlayer)).BeginInit();
            this.PlayerPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.VolumeButton)).BeginInit();
            this.SpeedPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SpeedDownButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SpeedUpButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PlayButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ProgressBar)).BeginInit();
            this.SuspendLayout();
            // 
            // ContainerPanel
            // 
            this.ContainerPanel.Controls.Add(this.VideoPanel);
            this.ContainerPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ContainerPanel.Location = new System.Drawing.Point(0, 0);
            this.ContainerPanel.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ContainerPanel.Name = "ContainerPanel";
            this.ContainerPanel.Size = new System.Drawing.Size(1263, 688);
            this.ContainerPanel.TabIndex = 2;
            // 
            // VideoPanel
            // 
            this.VideoPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(30)))), ((int)(((byte)(45)))));
            this.VideoPanel.Controls.Add(this.VolumeBar);
            this.VideoPanel.Controls.Add(this.VideoPlayAreaPanel);
            this.VideoPanel.Controls.Add(this.PlayerPanel);
            this.VideoPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.VideoPanel.Location = new System.Drawing.Point(0, 0);
            this.VideoPanel.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.VideoPanel.Name = "VideoPanel";
            this.VideoPanel.Size = new System.Drawing.Size(1263, 688);
            this.VideoPanel.TabIndex = 2;
            // 
            // VolumeBar
            // 
            this.VolumeBar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.VolumeBar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(21)))), ((int)(((byte)(32)))));
            this.VolumeBar.Location = new System.Drawing.Point(106, 460);
            this.VolumeBar.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.VolumeBar.Maximum = 50;
            this.VolumeBar.Name = "VolumeBar";
            this.VolumeBar.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.VolumeBar.Size = new System.Drawing.Size(69, 158);
            this.VolumeBar.TabIndex = 14;
            this.VolumeBar.TickStyle = System.Windows.Forms.TickStyle.None;
            this.VolumeBar.Visible = false;
            this.VolumeBar.Scroll += new System.EventHandler(this.VolumeBar_Scroll);
            // 
            // VideoPlayAreaPanel
            // 
            this.VideoPlayAreaPanel.Controls.Add(this.AxVideoPlayer);
            this.VideoPlayAreaPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.VideoPlayAreaPanel.Location = new System.Drawing.Point(0, 0);
            this.VideoPlayAreaPanel.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.VideoPlayAreaPanel.Name = "VideoPlayAreaPanel";
            this.VideoPlayAreaPanel.Size = new System.Drawing.Size(1263, 558);
            this.VideoPlayAreaPanel.TabIndex = 13;
            this.VideoPlayAreaPanel.Resize += new System.EventHandler(this.VideoPlayAreaPanel_Resize);
            // 
            // AxVideoPlayer
            // 
            this.AxVideoPlayer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.AxVideoPlayer.Enabled = true;
            this.AxVideoPlayer.Location = new System.Drawing.Point(0, 0);
            this.AxVideoPlayer.Name = "AxVideoPlayer";
            this.AxVideoPlayer.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("AxVideoPlayer.OcxState")));
            this.AxVideoPlayer.Size = new System.Drawing.Size(1263, 558);
            this.AxVideoPlayer.TabIndex = 13;
            this.AxVideoPlayer.OnCompleted += new System.EventHandler(this.AxVideoPlayer_OnCompleted);
            this.AxVideoPlayer.OnPlaying += new AxMOVIEPLAYERLib._DMoviePlayerEvents_OnPlayingEventHandler(this.AxVideoPlayer_OnPlaying);
            // 
            // PlayerPanel
            // 
            this.PlayerPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(21)))), ((int)(((byte)(32)))));
            this.PlayerPanel.Controls.Add(this.VolumeButton);
            this.PlayerPanel.Controls.Add(this.EndTimeLable);
            this.PlayerPanel.Controls.Add(this.StartTimeLable);
            this.PlayerPanel.Controls.Add(this.SpeedPanel);
            this.PlayerPanel.Controls.Add(this.SpeedDownButton);
            this.PlayerPanel.Controls.Add(this.SpeedUpButton);
            this.PlayerPanel.Controls.Add(this.PlayButton);
            this.PlayerPanel.Controls.Add(this.ProgressBar);
            this.PlayerPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.PlayerPanel.Location = new System.Drawing.Point(0, 558);
            this.PlayerPanel.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.PlayerPanel.Name = "PlayerPanel";
            this.PlayerPanel.Size = new System.Drawing.Size(1263, 130);
            this.PlayerPanel.TabIndex = 2;
            // 
            // VolumeButton
            // 
            this.VolumeButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.VolumeButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.VolumeButton.Image = ((System.Drawing.Image)(resources.GetObject("VolumeButton.Image")));
            this.VolumeButton.Location = new System.Drawing.Point(106, 62);
            this.VolumeButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.VolumeButton.Name = "VolumeButton";
            this.VolumeButton.Size = new System.Drawing.Size(24, 24);
            this.VolumeButton.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.VolumeButton.TabIndex = 17;
            this.VolumeButton.TabStop = false;
            this.VolumeButton.Click += new System.EventHandler(this.VolumeButton_Click);
            // 
            // EndTimeLable
            // 
            this.EndTimeLable.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.EndTimeLable.AutoSize = true;
            this.EndTimeLable.ForeColor = System.Drawing.Color.LightGray;
            this.EndTimeLable.Location = new System.Drawing.Point(1169, 31);
            this.EndTimeLable.Name = "EndTimeLable";
            this.EndTimeLable.Size = new System.Drawing.Size(80, 18);
            this.EndTimeLable.TabIndex = 16;
            this.EndTimeLable.Text = "00:00:00";
            // 
            // StartTimeLable
            // 
            this.StartTimeLable.AutoSize = true;
            this.StartTimeLable.ForeColor = System.Drawing.Color.LightGray;
            this.StartTimeLable.Location = new System.Drawing.Point(7, 31);
            this.StartTimeLable.Name = "StartTimeLable";
            this.StartTimeLable.Size = new System.Drawing.Size(80, 18);
            this.StartTimeLable.TabIndex = 14;
            this.StartTimeLable.Text = "00:00:00";
            // 
            // SpeedPanel
            // 
            this.SpeedPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SpeedPanel.Controls.Add(this.SpeedComboBox);
            this.SpeedPanel.Location = new System.Drawing.Point(1060, 60);
            this.SpeedPanel.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.SpeedPanel.Name = "SpeedPanel";
            this.SpeedPanel.Size = new System.Drawing.Size(107, 44);
            this.SpeedPanel.TabIndex = 13;
            // 
            // SpeedComboBox
            // 
            this.SpeedComboBox.BackColor = System.Drawing.Color.Black;
            this.SpeedComboBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SpeedComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.SpeedComboBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SpeedComboBox.Font = new System.Drawing.Font("微软雅黑", 11F, System.Drawing.FontStyle.Bold);
            this.SpeedComboBox.ForeColor = System.Drawing.Color.White;
            this.SpeedComboBox.FormattingEnabled = true;
            this.SpeedComboBox.Items.AddRange(new object[] {
            "0.5X",
            "0.75X",
            "1.0X",
            "1.25X",
            "1.5X",
            "2.0X"});
            this.SpeedComboBox.Location = new System.Drawing.Point(0, 0);
            this.SpeedComboBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.SpeedComboBox.Name = "SpeedComboBox";
            this.SpeedComboBox.Size = new System.Drawing.Size(107, 38);
            this.SpeedComboBox.TabIndex = 12;
            this.SpeedComboBox.SelectedIndexChanged += new System.EventHandler(this.SpeedComboBox_SelectedIndexChanged);
            // 
            // SpeedDownButton
            // 
            this.SpeedDownButton.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.SpeedDownButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.SpeedDownButton.Image = global::Vbox.Properties.Resources.backwardfill;
            this.SpeedDownButton.Location = new System.Drawing.Point(544, 62);
            this.SpeedDownButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.SpeedDownButton.Name = "SpeedDownButton";
            this.SpeedDownButton.Size = new System.Drawing.Size(32, 32);
            this.SpeedDownButton.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.SpeedDownButton.TabIndex = 3;
            this.SpeedDownButton.TabStop = false;
            this.SpeedDownButton.Click += new System.EventHandler(this.SpeedDownButton_Click);
            // 
            // SpeedUpButton
            // 
            this.SpeedUpButton.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.SpeedUpButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.SpeedUpButton.Image = global::Vbox.Properties.Resources.play_forward_fill;
            this.SpeedUpButton.Location = new System.Drawing.Point(672, 62);
            this.SpeedUpButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.SpeedUpButton.Name = "SpeedUpButton";
            this.SpeedUpButton.Size = new System.Drawing.Size(32, 32);
            this.SpeedUpButton.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.SpeedUpButton.TabIndex = 2;
            this.SpeedUpButton.TabStop = false;
            this.SpeedUpButton.Click += new System.EventHandler(this.SpeedUpButton_Click);
            // 
            // PlayButton
            // 
            this.PlayButton.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.PlayButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.PlayButton.Image = global::Vbox.Properties.Resources.playfill;
            this.PlayButton.Location = new System.Drawing.Point(608, 62);
            this.PlayButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.PlayButton.Name = "PlayButton";
            this.PlayButton.Size = new System.Drawing.Size(32, 32);
            this.PlayButton.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.PlayButton.TabIndex = 1;
            this.PlayButton.TabStop = false;
            this.PlayButton.Click += new System.EventHandler(this.PlayButton_Click);
            // 
            // ProgressBar
            // 
            this.ProgressBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ProgressBar.Location = new System.Drawing.Point(92, 25);
            this.ProgressBar.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ProgressBar.Maximum = 5000;
            this.ProgressBar.Name = "ProgressBar";
            this.ProgressBar.Size = new System.Drawing.Size(1075, 69);
            this.ProgressBar.TabIndex = 15;
            this.ProgressBar.TickStyle = System.Windows.Forms.TickStyle.None;
            this.ProgressBar.Scroll += new System.EventHandler(this.ProgressBar_Scroll);
            // 
            // VideoPlayerView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(32)))), ((int)(((byte)(39)))));
            this.ClientSize = new System.Drawing.Size(1263, 688);
            this.Controls.Add(this.ContainerPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "VideoPlayerView";
            this.Text = "VideoPlayerView";
            this.ContainerPanel.ResumeLayout(false);
            this.VideoPanel.ResumeLayout(false);
            this.VideoPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.VolumeBar)).EndInit();
            this.VideoPlayAreaPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.AxVideoPlayer)).EndInit();
            this.PlayerPanel.ResumeLayout(false);
            this.PlayerPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.VolumeButton)).EndInit();
            this.SpeedPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SpeedDownButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SpeedUpButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PlayButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ProgressBar)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel ContainerPanel;
        private System.Windows.Forms.Panel VideoPanel;
        private System.Windows.Forms.Panel VideoPlayAreaPanel;
        private System.Windows.Forms.Panel PlayerPanel;
        private System.Windows.Forms.Panel SpeedPanel;
        private System.Windows.Forms.ComboBox SpeedComboBox;
        private System.Windows.Forms.PictureBox PlayButton;
        private System.Windows.Forms.Label StartTimeLable;
        private System.Windows.Forms.TrackBar ProgressBar;
        private System.Windows.Forms.Label EndTimeLable;
        private System.Windows.Forms.TrackBar VolumeBar;
        private AxMOVIEPLAYERLib.AxMoviePlayer AxVideoPlayer;
        private System.Windows.Forms.PictureBox SpeedDownButton;
        private System.Windows.Forms.PictureBox SpeedUpButton;
        private System.Windows.Forms.PictureBox VolumeButton;
    }
}
﻿namespace Vbox
{
    partial class EditorView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EditorView));
            this.PreviewPanel = new System.Windows.Forms.Panel();
            this.VideoPreviewWindow = new System.Windows.Forms.Panel();
            this.OptionsPanel = new System.Windows.Forms.Panel();
            this.TextClipEndNum = new System.Windows.Forms.NumericUpDown();
            this.TextClipStartNum = new System.Windows.Forms.NumericUpDown();
            this.TextYNum = new System.Windows.Forms.NumericUpDown();
            this.TextXNum = new System.Windows.Forms.NumericUpDown();
            this.ClipStartNum = new System.Windows.Forms.NumericUpDown();
            this.ClipEndNum = new System.Windows.Forms.NumericUpDown();
            this.ColorButton = new System.Windows.Forms.Button();
            this.FontButton = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.TextToAddTextBox = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.ClipVideoButton = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.AddTextButton = new System.Windows.Forms.Button();
            this.SaveButton = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.ScaleBar = new System.Windows.Forms.TrackBar();
            this.TimelinePanel = new System.Windows.Forms.Panel();
            this.AxTimelineControl = new AxTimelineAxLib.AxTimelineControl();
            this.VideoInfoPanel = new System.Windows.Forms.Panel();
            this.label12 = new System.Windows.Forms.Label();
            this.ProgressBar = new System.Windows.Forms.ProgressBar();
            this.Label1 = new System.Windows.Forms.Label();
            this.TimeLineScalingLabel = new System.Windows.Forms.Label();
            this.Label0 = new System.Windows.Forms.Label();
            this.PlayButton = new System.Windows.Forms.Button();
            this.PauseButton = new System.Windows.Forms.Button();
            this.VideoDurationData = new System.Windows.Forms.Label();
            this.VideoDurationTItle = new System.Windows.Forms.Label();
            this.VideoPlayOptionLabel = new System.Windows.Forms.Label();
            this.VideoHeightData = new System.Windows.Forms.Label();
            this.VideoWidthData = new System.Windows.Forms.Label();
            this.VideoNameData = new System.Windows.Forms.Label();
            this.VideoNameTitle = new System.Windows.Forms.Label();
            this.VideoHeightTitle = new System.Windows.Forms.Label();
            this.VideoWidthTitle = new System.Windows.Forms.Label();
            this.VideoInfoLabel = new System.Windows.Forms.Label();
            this.StopButton = new System.Windows.Forms.Button();
            this.FontDialog = new System.Windows.Forms.FontDialog();
            this.ColorDialog = new System.Windows.Forms.ColorDialog();
            this.PreviewPanel.SuspendLayout();
            this.OptionsPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TextClipEndNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextClipStartNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextYNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextXNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClipStartNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClipEndNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ScaleBar)).BeginInit();
            this.TimelinePanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AxTimelineControl)).BeginInit();
            this.VideoInfoPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // PreviewPanel
            // 
            this.PreviewPanel.BackColor = System.Drawing.Color.Red;
            this.PreviewPanel.Controls.Add(this.VideoPreviewWindow);
            this.PreviewPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PreviewPanel.Location = new System.Drawing.Point(240, 0);
            this.PreviewPanel.Name = "PreviewPanel";
            this.PreviewPanel.Size = new System.Drawing.Size(1029, 643);
            this.PreviewPanel.TabIndex = 3;
            // 
            // VideoPreviewWindow
            // 
            this.VideoPreviewWindow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(30)))), ((int)(((byte)(45)))));
            this.VideoPreviewWindow.Dock = System.Windows.Forms.DockStyle.Fill;
            this.VideoPreviewWindow.Location = new System.Drawing.Point(0, 0);
            this.VideoPreviewWindow.Name = "VideoPreviewWindow";
            this.VideoPreviewWindow.Size = new System.Drawing.Size(1029, 643);
            this.VideoPreviewWindow.TabIndex = 13;
            // 
            // OptionsPanel
            // 
            this.OptionsPanel.Controls.Add(this.TextClipEndNum);
            this.OptionsPanel.Controls.Add(this.TextClipStartNum);
            this.OptionsPanel.Controls.Add(this.TextYNum);
            this.OptionsPanel.Controls.Add(this.TextXNum);
            this.OptionsPanel.Controls.Add(this.ClipStartNum);
            this.OptionsPanel.Controls.Add(this.ClipEndNum);
            this.OptionsPanel.Controls.Add(this.ColorButton);
            this.OptionsPanel.Controls.Add(this.FontButton);
            this.OptionsPanel.Controls.Add(this.label10);
            this.OptionsPanel.Controls.Add(this.label11);
            this.OptionsPanel.Controls.Add(this.TextToAddTextBox);
            this.OptionsPanel.Controls.Add(this.label9);
            this.OptionsPanel.Controls.Add(this.label7);
            this.OptionsPanel.Controls.Add(this.label8);
            this.OptionsPanel.Controls.Add(this.label5);
            this.OptionsPanel.Controls.Add(this.label6);
            this.OptionsPanel.Controls.Add(this.ClipVideoButton);
            this.OptionsPanel.Controls.Add(this.label3);
            this.OptionsPanel.Controls.Add(this.label4);
            this.OptionsPanel.Controls.Add(this.AddTextButton);
            this.OptionsPanel.Dock = System.Windows.Forms.DockStyle.Right;
            this.OptionsPanel.Location = new System.Drawing.Point(1269, 0);
            this.OptionsPanel.Name = "OptionsPanel";
            this.OptionsPanel.Size = new System.Drawing.Size(240, 643);
            this.OptionsPanel.TabIndex = 1;
            // 
            // TextClipEndNum
            // 
            this.TextClipEndNum.DecimalPlaces = 2;
            this.TextClipEndNum.Font = new System.Drawing.Font("微软雅黑", 8F);
            this.TextClipEndNum.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.TextClipEndNum.Location = new System.Drawing.Point(60, 421);
            this.TextClipEndNum.Maximum = new decimal(new int[] {
            -1593835520,
            466537709,
            54210,
            0});
            this.TextClipEndNum.Name = "TextClipEndNum";
            this.TextClipEndNum.Size = new System.Drawing.Size(120, 29);
            this.TextClipEndNum.TabIndex = 66;
            // 
            // TextClipStartNum
            // 
            this.TextClipStartNum.DecimalPlaces = 2;
            this.TextClipStartNum.Font = new System.Drawing.Font("微软雅黑", 8F);
            this.TextClipStartNum.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.TextClipStartNum.Location = new System.Drawing.Point(61, 384);
            this.TextClipStartNum.Maximum = new decimal(new int[] {
            -1593835520,
            466537709,
            54210,
            0});
            this.TextClipStartNum.Name = "TextClipStartNum";
            this.TextClipStartNum.Size = new System.Drawing.Size(120, 29);
            this.TextClipStartNum.TabIndex = 65;
            // 
            // TextYNum
            // 
            this.TextYNum.Font = new System.Drawing.Font("微软雅黑", 8F);
            this.TextYNum.Location = new System.Drawing.Point(61, 347);
            this.TextYNum.Maximum = new decimal(new int[] {
            -1593835520,
            466537709,
            54210,
            0});
            this.TextYNum.Name = "TextYNum";
            this.TextYNum.Size = new System.Drawing.Size(120, 29);
            this.TextYNum.TabIndex = 64;
            // 
            // TextXNum
            // 
            this.TextXNum.Font = new System.Drawing.Font("微软雅黑", 8F);
            this.TextXNum.Location = new System.Drawing.Point(61, 313);
            this.TextXNum.Maximum = new decimal(new int[] {
            -1593835520,
            466537709,
            54210,
            0});
            this.TextXNum.Name = "TextXNum";
            this.TextXNum.Size = new System.Drawing.Size(120, 29);
            this.TextXNum.TabIndex = 63;
            // 
            // ClipStartNum
            // 
            this.ClipStartNum.DecimalPlaces = 2;
            this.ClipStartNum.Font = new System.Drawing.Font("微软雅黑", 8F);
            this.ClipStartNum.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.ClipStartNum.Location = new System.Drawing.Point(59, 42);
            this.ClipStartNum.Maximum = new decimal(new int[] {
            -1593835520,
            466537709,
            54210,
            0});
            this.ClipStartNum.Name = "ClipStartNum";
            this.ClipStartNum.Size = new System.Drawing.Size(120, 29);
            this.ClipStartNum.TabIndex = 62;
            // 
            // ClipEndNum
            // 
            this.ClipEndNum.DecimalPlaces = 2;
            this.ClipEndNum.Font = new System.Drawing.Font("微软雅黑", 8F);
            this.ClipEndNum.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.ClipEndNum.Location = new System.Drawing.Point(59, 77);
            this.ClipEndNum.Maximum = new decimal(new int[] {
            -1593835520,
            466537709,
            54210,
            0});
            this.ClipEndNum.Name = "ClipEndNum";
            this.ClipEndNum.Size = new System.Drawing.Size(120, 29);
            this.ClipEndNum.TabIndex = 61;
            // 
            // ColorButton
            // 
            this.ColorButton.Font = new System.Drawing.Font("微软雅黑", 6F);
            this.ColorButton.Location = new System.Drawing.Point(69, 242);
            this.ColorButton.Name = "ColorButton";
            this.ColorButton.Size = new System.Drawing.Size(51, 28);
            this.ColorButton.TabIndex = 58;
            this.ColorButton.Text = "颜色";
            this.ColorButton.UseVisualStyleBackColor = true;
            this.ColorButton.Click += new System.EventHandler(this.ColorButton_Click);
            // 
            // FontButton
            // 
            this.FontButton.Font = new System.Drawing.Font("微软雅黑", 6F);
            this.FontButton.Location = new System.Drawing.Point(13, 242);
            this.FontButton.Name = "FontButton";
            this.FontButton.Size = new System.Drawing.Size(50, 28);
            this.FontButton.TabIndex = 57;
            this.FontButton.Text = "字体";
            this.FontButton.UseVisualStyleBackColor = true;
            this.FontButton.Click += new System.EventHandler(this.FontButton_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("微软雅黑", 8F);
            this.label10.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label10.Location = new System.Drawing.Point(9, 426);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(46, 21);
            this.label10.TabIndex = 48;
            this.label10.Text = "终点:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("微软雅黑", 8F);
            this.label11.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label11.Location = new System.Drawing.Point(9, 388);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(46, 21);
            this.label11.TabIndex = 47;
            this.label11.Text = "起点:";
            // 
            // TextToAddTextBox
            // 
            this.TextToAddTextBox.Font = new System.Drawing.Font("微软雅黑", 7F);
            this.TextToAddTextBox.Location = new System.Drawing.Point(61, 278);
            this.TextToAddTextBox.Name = "TextToAddTextBox";
            this.TextToAddTextBox.Size = new System.Drawing.Size(166, 26);
            this.TextToAddTextBox.TabIndex = 46;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("微软雅黑", 8F);
            this.label9.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label9.Location = new System.Drawing.Point(9, 280);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(46, 21);
            this.label9.TabIndex = 45;
            this.label9.Text = "内容:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("微软雅黑", 8F);
            this.label7.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label7.Location = new System.Drawing.Point(32, 349);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(22, 21);
            this.label7.TabIndex = 42;
            this.label7.Text = "y:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("微软雅黑", 8F);
            this.label8.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label8.Location = new System.Drawing.Point(32, 315);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(22, 21);
            this.label8.TabIndex = 41;
            this.label8.Text = "x:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("微软雅黑", 8F);
            this.label5.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label5.Location = new System.Drawing.Point(8, 77);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(46, 21);
            this.label5.TabIndex = 37;
            this.label5.Text = "终点:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("微软雅黑", 8F);
            this.label6.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label6.Location = new System.Drawing.Point(7, 44);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(46, 21);
            this.label6.TabIndex = 36;
            this.label6.Text = "起点:";
            // 
            // ClipVideoButton
            // 
            this.ClipVideoButton.Font = new System.Drawing.Font("微软雅黑", 6F);
            this.ClipVideoButton.Location = new System.Drawing.Point(11, 112);
            this.ClipVideoButton.Name = "ClipVideoButton";
            this.ClipVideoButton.Size = new System.Drawing.Size(79, 28);
            this.ClipVideoButton.TabIndex = 38;
            this.ClipVideoButton.Text = "确认裁剪";
            this.ClipVideoButton.UseVisualStyleBackColor = true;
            this.ClipVideoButton.Click += new System.EventHandler(this.ClipVideoButton_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Bold);
            this.label3.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label3.Location = new System.Drawing.Point(7, 207);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(84, 25);
            this.label3.TabIndex = 37;
            this.label3.Text = "添加文字";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Bold);
            this.label4.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label4.Location = new System.Drawing.Point(6, 12);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(48, 25);
            this.label4.TabIndex = 36;
            this.label4.Text = "裁剪";
            // 
            // AddTextButton
            // 
            this.AddTextButton.Font = new System.Drawing.Font("微软雅黑", 6F);
            this.AddTextButton.Location = new System.Drawing.Point(14, 463);
            this.AddTextButton.Name = "AddTextButton";
            this.AddTextButton.Size = new System.Drawing.Size(79, 28);
            this.AddTextButton.TabIndex = 37;
            this.AddTextButton.Text = "确认添加";
            this.AddTextButton.UseVisualStyleBackColor = true;
            this.AddTextButton.Click += new System.EventHandler(this.AddTextButton_Click);
            // 
            // SaveButton
            // 
            this.SaveButton.Font = new System.Drawing.Font("微软雅黑", 6F);
            this.SaveButton.Location = new System.Drawing.Point(13, 495);
            this.SaveButton.Name = "SaveButton";
            this.SaveButton.Size = new System.Drawing.Size(78, 28);
            this.SaveButton.TabIndex = 56;
            this.SaveButton.Text = "确认保存";
            this.SaveButton.UseVisualStyleBackColor = true;
            this.SaveButton.Click += new System.EventHandler(this.SaveButton_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Bold);
            this.label2.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label2.Location = new System.Drawing.Point(7, 429);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 25);
            this.label2.TabIndex = 38;
            this.label2.Text = "保存";
            // 
            // ScaleBar
            // 
            this.ScaleBar.Location = new System.Drawing.Point(8, 328);
            this.ScaleBar.Maximum = 100;
            this.ScaleBar.Minimum = 1;
            this.ScaleBar.Name = "ScaleBar";
            this.ScaleBar.Size = new System.Drawing.Size(222, 69);
            this.ScaleBar.TabIndex = 31;
            this.ScaleBar.TickStyle = System.Windows.Forms.TickStyle.None;
            this.ScaleBar.Value = 100;
            this.ScaleBar.Scroll += new System.EventHandler(this.ScaleBar_Scroll);
            // 
            // TimelinePanel
            // 
            this.TimelinePanel.Controls.Add(this.AxTimelineControl);
            this.TimelinePanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.TimelinePanel.Location = new System.Drawing.Point(0, 643);
            this.TimelinePanel.Name = "TimelinePanel";
            this.TimelinePanel.Size = new System.Drawing.Size(1509, 251);
            this.TimelinePanel.TabIndex = 4;
            // 
            // AxTimelineControl
            // 
            this.AxTimelineControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.AxTimelineControl.Enabled = true;
            this.AxTimelineControl.Location = new System.Drawing.Point(0, 0);
            this.AxTimelineControl.Name = "AxTimelineControl";
            this.AxTimelineControl.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("AxTimelineControl.OcxState")));
            this.AxTimelineControl.Size = new System.Drawing.Size(1509, 251);
            this.AxTimelineControl.TabIndex = 0;
            this.AxTimelineControl.OnConvertProgress += new AxTimelineAxLib._ITimelineControlEvents_OnConvertProgressEventHandler(this.AxTimelineControl_OnConvertProgress);
            this.AxTimelineControl.OnConvertCompleted += new System.EventHandler(this.AxTimelineControl_OnConvertCompleted);
            // 
            // VideoInfoPanel
            // 
            this.VideoInfoPanel.Controls.Add(this.label12);
            this.VideoInfoPanel.Controls.Add(this.ProgressBar);
            this.VideoInfoPanel.Controls.Add(this.label2);
            this.VideoInfoPanel.Controls.Add(this.Label1);
            this.VideoInfoPanel.Controls.Add(this.TimeLineScalingLabel);
            this.VideoInfoPanel.Controls.Add(this.Label0);
            this.VideoInfoPanel.Controls.Add(this.SaveButton);
            this.VideoInfoPanel.Controls.Add(this.PlayButton);
            this.VideoInfoPanel.Controls.Add(this.ScaleBar);
            this.VideoInfoPanel.Controls.Add(this.PauseButton);
            this.VideoInfoPanel.Controls.Add(this.VideoDurationData);
            this.VideoInfoPanel.Controls.Add(this.VideoDurationTItle);
            this.VideoInfoPanel.Controls.Add(this.VideoPlayOptionLabel);
            this.VideoInfoPanel.Controls.Add(this.VideoHeightData);
            this.VideoInfoPanel.Controls.Add(this.VideoWidthData);
            this.VideoInfoPanel.Controls.Add(this.VideoNameData);
            this.VideoInfoPanel.Controls.Add(this.VideoNameTitle);
            this.VideoInfoPanel.Controls.Add(this.VideoHeightTitle);
            this.VideoInfoPanel.Controls.Add(this.VideoWidthTitle);
            this.VideoInfoPanel.Controls.Add(this.VideoInfoLabel);
            this.VideoInfoPanel.Controls.Add(this.StopButton);
            this.VideoInfoPanel.Dock = System.Windows.Forms.DockStyle.Left;
            this.VideoInfoPanel.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.VideoInfoPanel.Location = new System.Drawing.Point(0, 0);
            this.VideoInfoPanel.Name = "VideoInfoPanel";
            this.VideoInfoPanel.Size = new System.Drawing.Size(240, 643);
            this.VideoInfoPanel.TabIndex = 2;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("微软雅黑", 8F);
            this.label12.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label12.Location = new System.Drawing.Point(9, 464);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(46, 21);
            this.label12.TabIndex = 60;
            this.label12.Text = "进度:";
            // 
            // ProgressBar
            // 
            this.ProgressBar.Location = new System.Drawing.Point(65, 460);
            this.ProgressBar.Margin = new System.Windows.Forms.Padding(4);
            this.ProgressBar.Name = "ProgressBar";
            this.ProgressBar.Size = new System.Drawing.Size(155, 28);
            this.ProgressBar.TabIndex = 59;
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Font = new System.Drawing.Font("微软雅黑", 7F, System.Drawing.FontStyle.Bold);
            this.Label1.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.Label1.Location = new System.Drawing.Point(202, 360);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(18, 19);
            this.Label1.TabIndex = 35;
            this.Label1.Text = "1";
            // 
            // TimeLineScalingLabel
            // 
            this.TimeLineScalingLabel.AutoSize = true;
            this.TimeLineScalingLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.TimeLineScalingLabel.Location = new System.Drawing.Point(3, 300);
            this.TimeLineScalingLabel.Name = "TimeLineScalingLabel";
            this.TimeLineScalingLabel.Size = new System.Drawing.Size(102, 25);
            this.TimeLineScalingLabel.TabIndex = 33;
            this.TimeLineScalingLabel.Text = "时间轴缩放";
            // 
            // Label0
            // 
            this.Label0.AutoSize = true;
            this.Label0.Font = new System.Drawing.Font("微软雅黑", 7F, System.Drawing.FontStyle.Bold);
            this.Label0.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.Label0.Location = new System.Drawing.Point(15, 353);
            this.Label0.Name = "Label0";
            this.Label0.Size = new System.Drawing.Size(18, 19);
            this.Label0.TabIndex = 34;
            this.Label0.Text = "0";
            // 
            // PlayButton
            // 
            this.PlayButton.Font = new System.Drawing.Font("微软雅黑", 7F);
            this.PlayButton.Location = new System.Drawing.Point(12, 203);
            this.PlayButton.Name = "PlayButton";
            this.PlayButton.Size = new System.Drawing.Size(66, 34);
            this.PlayButton.TabIndex = 29;
            this.PlayButton.Text = "播放";
            this.PlayButton.UseVisualStyleBackColor = true;
            this.PlayButton.Click += new System.EventHandler(this.PlayButton_Click);
            // 
            // PauseButton
            // 
            this.PauseButton.Font = new System.Drawing.Font("微软雅黑", 7F);
            this.PauseButton.Location = new System.Drawing.Point(84, 203);
            this.PauseButton.Name = "PauseButton";
            this.PauseButton.Size = new System.Drawing.Size(66, 34);
            this.PauseButton.TabIndex = 27;
            this.PauseButton.Text = "暂停";
            this.PauseButton.UseVisualStyleBackColor = true;
            this.PauseButton.Click += new System.EventHandler(this.PauseButton_Click);
            // 
            // VideoDurationData
            // 
            this.VideoDurationData.AutoSize = true;
            this.VideoDurationData.Font = new System.Drawing.Font("微软雅黑", 8F);
            this.VideoDurationData.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.VideoDurationData.Location = new System.Drawing.Point(61, 67);
            this.VideoDurationData.Name = "VideoDurationData";
            this.VideoDurationData.Size = new System.Drawing.Size(42, 21);
            this.VideoDurationData.TabIndex = 26;
            this.VideoDurationData.Text = "名称";
            // 
            // VideoDurationTItle
            // 
            this.VideoDurationTItle.AutoSize = true;
            this.VideoDurationTItle.Font = new System.Drawing.Font("微软雅黑", 8F);
            this.VideoDurationTItle.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.VideoDurationTItle.Location = new System.Drawing.Point(13, 67);
            this.VideoDurationTItle.Name = "VideoDurationTItle";
            this.VideoDurationTItle.Size = new System.Drawing.Size(46, 21);
            this.VideoDurationTItle.TabIndex = 25;
            this.VideoDurationTItle.Text = "时长:";
            // 
            // VideoPlayOptionLabel
            // 
            this.VideoPlayOptionLabel.AutoSize = true;
            this.VideoPlayOptionLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.VideoPlayOptionLabel.Location = new System.Drawing.Point(3, 167);
            this.VideoPlayOptionLabel.Name = "VideoPlayOptionLabel";
            this.VideoPlayOptionLabel.Size = new System.Drawing.Size(84, 25);
            this.VideoPlayOptionLabel.TabIndex = 24;
            this.VideoPlayOptionLabel.Text = "预览操作";
            // 
            // VideoHeightData
            // 
            this.VideoHeightData.AutoSize = true;
            this.VideoHeightData.Font = new System.Drawing.Font("微软雅黑", 8F);
            this.VideoHeightData.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.VideoHeightData.Location = new System.Drawing.Point(152, 93);
            this.VideoHeightData.Name = "VideoHeightData";
            this.VideoHeightData.Size = new System.Drawing.Size(42, 21);
            this.VideoHeightData.TabIndex = 21;
            this.VideoHeightData.Text = "名称";
            // 
            // VideoWidthData
            // 
            this.VideoWidthData.AutoSize = true;
            this.VideoWidthData.Font = new System.Drawing.Font("微软雅黑", 8F);
            this.VideoWidthData.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.VideoWidthData.Location = new System.Drawing.Point(61, 93);
            this.VideoWidthData.Name = "VideoWidthData";
            this.VideoWidthData.Size = new System.Drawing.Size(42, 21);
            this.VideoWidthData.TabIndex = 20;
            this.VideoWidthData.Text = "名称";
            // 
            // VideoNameData
            // 
            this.VideoNameData.AutoSize = true;
            this.VideoNameData.Font = new System.Drawing.Font("微软雅黑", 8F);
            this.VideoNameData.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.VideoNameData.Location = new System.Drawing.Point(61, 42);
            this.VideoNameData.MaximumSize = new System.Drawing.Size(159, 21);
            this.VideoNameData.Name = "VideoNameData";
            this.VideoNameData.Size = new System.Drawing.Size(42, 21);
            this.VideoNameData.TabIndex = 19;
            this.VideoNameData.Text = "名称";
            // 
            // VideoNameTitle
            // 
            this.VideoNameTitle.AutoSize = true;
            this.VideoNameTitle.Font = new System.Drawing.Font("微软雅黑", 8F);
            this.VideoNameTitle.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.VideoNameTitle.Location = new System.Drawing.Point(13, 42);
            this.VideoNameTitle.Name = "VideoNameTitle";
            this.VideoNameTitle.Size = new System.Drawing.Size(46, 21);
            this.VideoNameTitle.TabIndex = 18;
            this.VideoNameTitle.Text = "名称:";
            // 
            // VideoHeightTitle
            // 
            this.VideoHeightTitle.AutoSize = true;
            this.VideoHeightTitle.Font = new System.Drawing.Font("微软雅黑", 8F);
            this.VideoHeightTitle.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.VideoHeightTitle.Location = new System.Drawing.Point(120, 93);
            this.VideoHeightTitle.Name = "VideoHeightTitle";
            this.VideoHeightTitle.Size = new System.Drawing.Size(30, 21);
            this.VideoHeightTitle.TabIndex = 17;
            this.VideoHeightTitle.Text = "高:";
            // 
            // VideoWidthTitle
            // 
            this.VideoWidthTitle.AutoSize = true;
            this.VideoWidthTitle.Font = new System.Drawing.Font("微软雅黑", 8F);
            this.VideoWidthTitle.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.VideoWidthTitle.Location = new System.Drawing.Point(29, 93);
            this.VideoWidthTitle.Name = "VideoWidthTitle";
            this.VideoWidthTitle.Size = new System.Drawing.Size(30, 21);
            this.VideoWidthTitle.TabIndex = 16;
            this.VideoWidthTitle.Text = "宽:";
            // 
            // VideoInfoLabel
            // 
            this.VideoInfoLabel.AutoSize = true;
            this.VideoInfoLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.VideoInfoLabel.Location = new System.Drawing.Point(3, 9);
            this.VideoInfoLabel.Name = "VideoInfoLabel";
            this.VideoInfoLabel.Size = new System.Drawing.Size(84, 25);
            this.VideoInfoLabel.TabIndex = 15;
            this.VideoInfoLabel.Text = "视频信息";
            // 
            // StopButton
            // 
            this.StopButton.Font = new System.Drawing.Font("微软雅黑", 7F);
            this.StopButton.Location = new System.Drawing.Point(156, 203);
            this.StopButton.Name = "StopButton";
            this.StopButton.Size = new System.Drawing.Size(66, 34);
            this.StopButton.TabIndex = 14;
            this.StopButton.Text = "停止";
            this.StopButton.UseVisualStyleBackColor = true;
            this.StopButton.Click += new System.EventHandler(this.StopButton_Click);
            // 
            // EditorView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 24F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(32)))), ((int)(((byte)(39)))));
            this.ClientSize = new System.Drawing.Size(1509, 894);
            this.Controls.Add(this.PreviewPanel);
            this.Controls.Add(this.VideoInfoPanel);
            this.Controls.Add(this.OptionsPanel);
            this.Controls.Add(this.TimelinePanel);
            this.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.Name = "EditorView";
            this.Text = "EditorView";
            this.Load += new System.EventHandler(this.EditorView_Load);
            this.PreviewPanel.ResumeLayout(false);
            this.OptionsPanel.ResumeLayout(false);
            this.OptionsPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TextClipEndNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextClipStartNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextYNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextXNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClipStartNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClipEndNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ScaleBar)).EndInit();
            this.TimelinePanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.AxTimelineControl)).EndInit();
            this.VideoInfoPanel.ResumeLayout(false);
            this.VideoInfoPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel PreviewPanel;
        private System.Windows.Forms.Panel VideoPreviewWindow;
        private System.Windows.Forms.Panel OptionsPanel;
        private System.Windows.Forms.Panel TimelinePanel;
        private AxTimelineAxLib.AxTimelineControl AxTimelineControl;
        private System.Windows.Forms.Panel VideoInfoPanel;
        private System.Windows.Forms.TrackBar ScaleBar;
        private System.Windows.Forms.Button StopButton;
        private System.Windows.Forms.Label VideoInfoLabel;
        private System.Windows.Forms.Label VideoHeightData;
        private System.Windows.Forms.Label VideoWidthData;
        private System.Windows.Forms.Label VideoNameData;
        private System.Windows.Forms.Label VideoNameTitle;
        private System.Windows.Forms.Label VideoHeightTitle;
        private System.Windows.Forms.Label VideoWidthTitle;
        private System.Windows.Forms.Label VideoDurationData;
        private System.Windows.Forms.Label VideoDurationTItle;
        private System.Windows.Forms.Label VideoPlayOptionLabel;
        private System.Windows.Forms.Button PauseButton;
        private System.Windows.Forms.Button PlayButton;
        private System.Windows.Forms.Label Label1;
        private System.Windows.Forms.Label TimeLineScalingLabel;
        private System.Windows.Forms.Label Label0;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button ClipVideoButton;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button AddTextButton;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox TextToAddTextBox;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button SaveButton;
        private System.Windows.Forms.FontDialog FontDialog;
        private System.Windows.Forms.ColorDialog ColorDialog;
        private System.Windows.Forms.Button ColorButton;
        private System.Windows.Forms.Button FontButton;
        internal System.Windows.Forms.ProgressBar ProgressBar;
        private System.Windows.Forms.NumericUpDown ClipStartNum;
        private System.Windows.Forms.NumericUpDown ClipEndNum;
        private System.Windows.Forms.NumericUpDown TextClipEndNum;
        private System.Windows.Forms.NumericUpDown TextClipStartNum;
        private System.Windows.Forms.NumericUpDown TextYNum;
        private System.Windows.Forms.NumericUpDown TextXNum;
        private System.Windows.Forms.Label label12;
    }
}
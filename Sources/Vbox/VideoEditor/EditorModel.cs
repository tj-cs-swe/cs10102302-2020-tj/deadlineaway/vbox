﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using Font = System.Drawing.Font;

namespace Vbox
{
    public class EditorModel
    {
        public FileModel File { get; private set; }

        public double ClipStart { get; private set; } = -1;
        public double ClipEnd { get; private set; } = -1;

        public String Text { get; private set; } = null;
        public int TextX { get; private set; } = -1;
        public int TextY { get; private set; } = -1;
        public double TextClipStart { get; private set; } = -1;
        public double TextClipEnd { get; private set; } = -1;

        public Font TextFont { get; private set; } = null;
        public Color TextColor { get; private set; } = Color.Empty;

        public bool IsClipped { get => ClipStart != 1 && ClipEnd != 1; }

        public String SavePath = null;

        public bool IsTextAdded
        {
            get =>
                Text != null && TextX != -1 && TextY != -1 &&
                TextClipStart != -1 && TextClipEnd != -1;
        }

        public double ScaleRate = 1.0;

        public EditorModel(FileModel file)
        {
            File = file;
        }

        public void Clip(double clipStart, double clipEnd)
        {
            ClipStart = clipStart;
            ClipEnd = clipEnd;
        }

        public void AddText(String text, int x, int y, double clipStart, double clipEnd, Font textFont, Color textColor)
        {
            Text = text;
            TextX = x;
            TextY = y;
            TextClipStart = clipStart;
            TextClipEnd = clipEnd;
            TextFont = textFont;
            TextColor = textColor;
        }

        public void UnClip()
        {
            ClipStart = -1;
            ClipEnd = -1;
        }

        public void RemoveAddedText()
        {
            Text = null;
            TextX = -1;
            TextY = -1;
        }

        public enum State
        {
            Previewing, Stopped, Paused
        }

        public State PlayState { get; private set; } = State.Stopped;

        public void Preview()
        {
            PlayState = State.Previewing;
        }

        public void Stop()
        {
            PlayState = State.Stopped;
        }

        public void Pause()
        {
            PlayState = State.Paused;
        }
    }
}

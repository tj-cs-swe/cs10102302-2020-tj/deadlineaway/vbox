﻿using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace Vbox
{
    public partial class EditorView : Form
    {
        private EditorModel Editor;
        private RepositoryModel Repository;

        public EditorView(RepositoryModel repository)
        {
            Repository = repository;
            Editor = new EditorModel(repository.SelectedFile);
            InitializeComponent();
        }

        private void EditorView_Load(object sender, EventArgs e)
        {
            AxTimelineControl.SetPreviewWnd((int)VideoPreviewWindow.Handle);
            UpdateVideoInfoFromModelAndAxTimeLine();
            UpdateSelectedFileViewFromModel();
            AxTimelineControl.SetVideoTrackFrameRate(30);
            AxTimelineControl.SetVideoTrackResolution(720, 480);
        }

        private void UpdateVideoInfoFromModelAndAxTimeLine()
        {
            var Path = Editor.File.FullPath;
            var Duration = AxTimelineControl.GetMediaDuration(Path);
            var DurationDateTime = DateTime.Parse(DateTime.Now.ToString("00:00:00")).AddSeconds(Duration);
            var DurationText = String.Format("{0:HH:mm:ss}", DurationDateTime);
            var WidthText = AxTimelineControl.GetMediaHeight(Path).ToString();
            var HeightText = AxTimelineControl.GetMediaWidth(Path).ToString();

            VideoNameData.Text = Editor.File.Name;
            VideoDurationData.Text = DurationText;
            VideoWidthData.Text = WidthText;
            VideoHeightData.Text = HeightText;
        }

        private void StopButton_Click(object sender, EventArgs e)
        {
            Editor.Stop();
            UpdatePlayStateFromModel();
        }

        private void PlayButton_Click(object sender, EventArgs e)
        {
            Editor.Preview();
            UpdatePlayStateFromModel();
        }


        private void UpdateVideoClipFromModel()
        {
            AxTimelineControl.ChangeVideoClip(
                1, 0, Editor.File.FullPath, (float)Editor.ClipStart, (float)Editor.ClipEnd, 0
            );
            AxTimelineControl.ChangeAudioClip(
                5, 0, Editor.File.FullPath, (float)Editor.ClipStart, (float)Editor.ClipEnd, 0, 1
            );
        }

        void UpdateVideoTextFromModel()
        {
            AxTimelineControl.AddTextClip(
                2, Editor.Text, (float)Editor.TextClipStart, (float)Editor.TextClipEnd,
                (int)Editor.TextFont.ToHfont(), Editor.TextX, Editor.TextY,
                Color2Uint32(Editor.TextColor)
            );

            uint Color2Uint32(Color color)
            {

                int t;
                byte[] a;

                t = ColorTranslator.ToOle(color);
                a = BitConverter.GetBytes(t);
                return BitConverter.ToUInt32(a, 0);

            }
        }

        private void UpdatePlayStateFromModel()
        {
            switch (Editor.PlayState)
            {
                case EditorModel.State.Previewing:
                    AxTimelineControl.SetPreviewWnd((int)VideoPreviewWindow.Handle);
                    AxTimelineControl.Play();
                    break;
                case EditorModel.State.Stopped:
                    AxTimelineControl.Stop();
                    break;
                case EditorModel.State.Paused:
                    AxTimelineControl.Pause();
                    break;
            }
        }

        private void UpdateSelectedFileViewFromModel()
        {
            AxTimelineControl.Clear();
            //LabelTitle.Text = SelectedFile.Name;
            var Path = Editor.File.FullPath;
            var Duration = AxTimelineControl.GetMediaDuration(Path);
            AxTimelineControl.AddVideoClip(1, Path, 0, Duration, 0, 2);
            AxTimelineControl.AddAudioClip(5, Path, 0, Duration, 0, 1);
        }

        void UpdateSavingStateFromModel()
        {

            AxTimelineControl.MP4Framerate = 25;

            AxTimelineControl.MP4Height = 480;
            AxTimelineControl.MP4Width = 720;
            AxTimelineControl.MP4VideoBitrate = 5000000;

            if (AxTimelineControl.Save(Editor.SavePath) != 1)
            {
                MessageBox.Show("保存失败");
            }

            AxTimelineControl.Stop();
        }

        //private void ClipVideoButton_Click(object sender, EventArgs e)
        //{
        //    var DialogResult = new ClipDialog(Editor).ShowDialog();
        //    if (DialogResult == DialogResult.OK)
        //    {
        //        UpdateVideoClipFromModel();
        //    }
        //}

        //private void SaveButton_Click(object sender, EventArgs e)
        //{
        //    var DialogResult = new SaveVideoDialog(Editor).ShowDialog();
        //    if (DialogResult == DialogResult.OK)
        //    {
        //        UpdateSavingStateFromModel();
        //    }
        //}

        //private void AddTextButton_Click(object sender, EventArgs e)
        //{
        //    var DialogResult = new AddTextDialog(Editor).ShowDialog();
        //    if (DialogResult == DialogResult.OK)
        //    {
        //        UpdateVideoTextFromModel();
        //    }
        //}

        private void ScaleBar_Scroll(object sender, EventArgs e)
        {
            Editor.ScaleRate = (double)ScaleBar.Value / (double)ScaleBar.Maximum;
            UpdateTimeLineScaleFromModel();
        }

        void UpdateTimeLineScaleFromModel() {
            AxTimelineControl.SetScale((float)Editor.ScaleRate);
        }

        void UpdateScaleBarFromModel()
        {
            ScaleBar.Value = (int)(ScaleBar.Maximum * Editor.ScaleRate);
        }

        private void PauseButton_Click(object sender, EventArgs e)
        {
            Editor.Pause();
            UpdatePlayStateFromModel();
        }

        private void ClipVideoButton_Click(object sender, EventArgs e)
        {
            var ClipStart = Convert.ToDouble(ClipStartNum.Value);
            var ClipEnd = Convert.ToDouble(ClipEndNum.Value);

            if (ClipRangeInvalid(ClipStart, ClipEnd)) { return; } ;

            Editor.Clip(ClipStart, ClipEnd);
            UpdateVideoClipFromModel();
        }

        private bool ClipRangeInvalid(double clipStart, double clipEnd)
        {
            if (clipStart >clipEnd)
            {
                MessageBox.Show("裁剪起点不能大于裁剪终点");
                return true;
            }


            var Duration = AxTimelineControl.GetMediaDuration(Editor.File.FullPath);
            if (clipStart > Duration ||clipEnd > Duration)
            {
                MessageBox.Show("裁剪的范围不能超过片长");
                return true;
            }

            return false;
        }

        private void AddTextButton_Click(object sender, EventArgs e)
        {
            var Text = TextToAddTextBox.Text;
            var X = Convert.ToInt32(TextXNum.Value);
            var Y = Convert.ToInt32(TextYNum.Value);
            var ClipStart = Convert.ToDouble(TextClipStartNum.Value);
            var ClipEnd = Convert.ToDouble(TextClipEndNum.Value);

            if (Text == null || Text.Length == 0)
            {
                MessageBox.Show("添加的文字不能为空");
                return;
            }

            if (ClipStart >= ClipEnd)
            {
                MessageBox.Show("文字添加的时间段长度必须大于 0");
                return;
            }


            var Duration = AxTimelineControl.GetMediaDuration(Editor.File.FullPath);
            if (ClipStart > Duration || ClipEnd > Duration)
            {
                MessageBox.Show("文字添加的范围不能超过片长");
                return;
            }

            var Width = AxTimelineControl.GetMediaWidth(Editor.File.FullPath);
            var Height = AxTimelineControl.GetMediaHeight(Editor.File.FullPath);

            if (X < 0 || Y < 0 || X > Width || Y > Height)
            {
                MessageBox.Show("添加的坐标需要在视频的长宽范围内");
                return;
            }

            Editor.AddText(Text, X, Y, ClipStart, ClipEnd, FontDialog.Font, ColorDialog.Color);
            UpdateVideoTextFromModel();
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            var Dialog = new SaveFileDialog();
            Dialog.Filter = "MP4 File (*.mp4)|*.mp4||";
            if (Dialog.ShowDialog() == DialogResult.OK)
            {
                Editor.SavePath = Dialog.FileName;
                UpdateSavingStateFromModel();
            } 
        }

        private void ColorButton_Click(object sender, EventArgs e)
        {
            ColorDialog.ShowDialog();
        }

        private void FontButton_Click(object sender, EventArgs e)
        {
            FontDialog.ShowDialog();
        }

        private void AxTimelineControl_OnConvertCompleted(object sender, EventArgs e)
        {
            MessageBox.Show("保存成功");
            Repository.AddFile(Editor.SavePath);
            this.DialogResult = DialogResult.OK;
        }

        private void AxTimelineControl_OnConvertProgress(object sender, AxTimelineAxLib._ITimelineControlEvents_OnConvertProgressEvent e)
        {
            ProgressBar.Value = e.progress;
        }
    }
}

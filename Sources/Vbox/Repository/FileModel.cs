﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vbox
{
    /// <summary>
    /// 代表操作系统中的某个文件
    /// </summary>
    public class FileModel
    {
        /// <summary>
        /// 文件信息类，这是标准库中的类，能得到文件信息，这里相当于一个代理
        /// </summary>
        private readonly FileInfo FileInfo;
        
        public bool Exists { get => File.Exists(FullPath); }

        public String FullPath
        {
            get
            {
                return FileInfo.FullName;
            }
        }

        public String Name
        {
            get
            {
                return FileInfo.Name;
            }
        }

        public DateTime LastWriteTime
        {
            get {
                return FileInfo.LastWriteTime;
            }
        }

        public int SizeKB
        {
            get
            {
                return (int)System.Math.Ceiling(FileInfo.Length / 1024.0);
            }
        }

        /// <summary>
        /// 根据路径初始化
        /// </summary>
        /// <param name="path">相对路径或者绝对路径</param>
        public FileModel(String path) {
            this.FileInfo = new FileInfo(path);
        }

        /// <summary>
        /// 将文件从操作系统中删除
        /// </summary>
        public void RemoveFromSystem()
        {
            File.Delete(FullPath);
        }
    }
}

﻿namespace Vbox
{
    partial class RepositoryView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RepositoryView));
            this.ContainerPanel = new System.Windows.Forms.Panel();
            this.DetailPanel = new System.Windows.Forms.Panel();
            this.ConvertButton = new System.Windows.Forms.Button();
            this.DeleteFromSystemButton = new System.Windows.Forms.Button();
            this.PathTitleLabel = new System.Windows.Forms.Label();
            this.PathDataLabel = new System.Windows.Forms.Label();
            this.DeleteButton = new System.Windows.Forms.Button();
            this.PlayButton = new System.Windows.Forms.Button();
            this.EditButton = new System.Windows.Forms.Button();
            this.AddButton = new System.Windows.Forms.Button();
            this.LeftPanel = new System.Windows.Forms.Panel();
            this.SearchResultPanel = new System.Windows.Forms.Panel();
            this.FileListView = new System.Windows.Forms.DataGridView();
            this.SearchPanel = new System.Windows.Forms.Panel();
            this.RecordButton = new System.Windows.Forms.Button();
            this.SearchBoxPanel = new System.Windows.Forms.Panel();
            this.SearchBarView = new System.Windows.Forms.TextBox();
            this.SearchIcon = new System.Windows.Forms.PictureBox();
            this.ContainerPanel.SuspendLayout();
            this.DetailPanel.SuspendLayout();
            this.LeftPanel.SuspendLayout();
            this.SearchResultPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FileListView)).BeginInit();
            this.SearchPanel.SuspendLayout();
            this.SearchBoxPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SearchIcon)).BeginInit();
            this.SuspendLayout();
            // 
            // ContainerPanel
            // 
            this.ContainerPanel.Controls.Add(this.DetailPanel);
            this.ContainerPanel.Controls.Add(this.LeftPanel);
            this.ContainerPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ContainerPanel.Location = new System.Drawing.Point(0, 0);
            this.ContainerPanel.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ContainerPanel.Name = "ContainerPanel";
            this.ContainerPanel.Size = new System.Drawing.Size(616, 811);
            this.ContainerPanel.TabIndex = 2;
            // 
            // DetailPanel
            // 
            this.DetailPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(30)))), ((int)(((byte)(45)))));
            this.DetailPanel.Controls.Add(this.ConvertButton);
            this.DetailPanel.Controls.Add(this.DeleteFromSystemButton);
            this.DetailPanel.Controls.Add(this.PathTitleLabel);
            this.DetailPanel.Controls.Add(this.PathDataLabel);
            this.DetailPanel.Controls.Add(this.DeleteButton);
            this.DetailPanel.Controls.Add(this.PlayButton);
            this.DetailPanel.Controls.Add(this.EditButton);
            this.DetailPanel.Controls.Add(this.AddButton);
            this.DetailPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.DetailPanel.Location = new System.Drawing.Point(0, 690);
            this.DetailPanel.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DetailPanel.Name = "DetailPanel";
            this.DetailPanel.Size = new System.Drawing.Size(616, 121);
            this.DetailPanel.TabIndex = 3;
            // 
            // ConvertButton
            // 
            this.ConvertButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(21)))), ((int)(((byte)(32)))));
            this.ConvertButton.FlatAppearance.BorderColor = System.Drawing.Color.DimGray;
            this.ConvertButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ConvertButton.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Bold);
            this.ConvertButton.ForeColor = System.Drawing.Color.White;
            this.ConvertButton.Location = new System.Drawing.Point(258, 66);
            this.ConvertButton.Margin = new System.Windows.Forms.Padding(5);
            this.ConvertButton.Name = "ConvertButton";
            this.ConvertButton.Size = new System.Drawing.Size(74, 41);
            this.ConvertButton.TabIndex = 8;
            this.ConvertButton.Text = "转码";
            this.ConvertButton.UseVisualStyleBackColor = false;
            this.ConvertButton.Click += new System.EventHandler(this.ConvertButton_Click);
            // 
            // DeleteFromSystemButton
            // 
            this.DeleteFromSystemButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(21)))), ((int)(((byte)(32)))));
            this.DeleteFromSystemButton.FlatAppearance.BorderColor = System.Drawing.Color.DimGray;
            this.DeleteFromSystemButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.DeleteFromSystemButton.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Bold);
            this.DeleteFromSystemButton.ForeColor = System.Drawing.Color.White;
            this.DeleteFromSystemButton.Location = new System.Drawing.Point(484, 67);
            this.DeleteFromSystemButton.Margin = new System.Windows.Forms.Padding(5);
            this.DeleteFromSystemButton.Name = "DeleteFromSystemButton";
            this.DeleteFromSystemButton.Size = new System.Drawing.Size(118, 41);
            this.DeleteFromSystemButton.TabIndex = 7;
            this.DeleteFromSystemButton.Text = "系统中删除";
            this.DeleteFromSystemButton.UseVisualStyleBackColor = false;
            this.DeleteFromSystemButton.Click += new System.EventHandler(this.DeleteFromSystemButton_Click);
            // 
            // PathTitleLabel
            // 
            this.PathTitleLabel.AutoSize = true;
            this.PathTitleLabel.Font = new System.Drawing.Font("微软雅黑", 8F, System.Drawing.FontStyle.Bold);
            this.PathTitleLabel.ForeColor = System.Drawing.Color.White;
            this.PathTitleLabel.Location = new System.Drawing.Point(14, 16);
            this.PathTitleLabel.Margin = new System.Windows.Forms.Padding(5);
            this.PathTitleLabel.Name = "PathTitleLabel";
            this.PathTitleLabel.Size = new System.Drawing.Size(47, 22);
            this.PathTitleLabel.TabIndex = 5;
            this.PathTitleLabel.Text = "路径:";
            // 
            // PathDataLabel
            // 
            this.PathDataLabel.AutoSize = true;
            this.PathDataLabel.Font = new System.Drawing.Font("微软雅黑", 8F, System.Drawing.FontStyle.Bold);
            this.PathDataLabel.ForeColor = System.Drawing.Color.White;
            this.PathDataLabel.Location = new System.Drawing.Point(65, 16);
            this.PathDataLabel.Margin = new System.Windows.Forms.Padding(5);
            this.PathDataLabel.MaximumSize = new System.Drawing.Size(398, 22);
            this.PathDataLabel.Name = "PathDataLabel";
            this.PathDataLabel.Size = new System.Drawing.Size(81, 22);
            this.PathDataLabel.TabIndex = 4;
            this.PathDataLabel.Text = "Full Path";
            // 
            // DeleteButton
            // 
            this.DeleteButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(21)))), ((int)(((byte)(32)))));
            this.DeleteButton.FlatAppearance.BorderColor = System.Drawing.Color.DimGray;
            this.DeleteButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.DeleteButton.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Bold);
            this.DeleteButton.ForeColor = System.Drawing.Color.White;
            this.DeleteButton.Location = new System.Drawing.Point(484, 16);
            this.DeleteButton.Margin = new System.Windows.Forms.Padding(5);
            this.DeleteButton.Name = "DeleteButton";
            this.DeleteButton.Size = new System.Drawing.Size(118, 41);
            this.DeleteButton.TabIndex = 3;
            this.DeleteButton.Text = "仓库中删除";
            this.DeleteButton.UseVisualStyleBackColor = false;
            this.DeleteButton.Click += new System.EventHandler(this.DeleteButton_Click);
            // 
            // PlayButton
            // 
            this.PlayButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(21)))), ((int)(((byte)(32)))));
            this.PlayButton.FlatAppearance.BorderColor = System.Drawing.Color.DimGray;
            this.PlayButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PlayButton.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Bold);
            this.PlayButton.ForeColor = System.Drawing.Color.White;
            this.PlayButton.Location = new System.Drawing.Point(94, 66);
            this.PlayButton.Margin = new System.Windows.Forms.Padding(5);
            this.PlayButton.Name = "PlayButton";
            this.PlayButton.Size = new System.Drawing.Size(70, 41);
            this.PlayButton.TabIndex = 2;
            this.PlayButton.Text = "播放";
            this.PlayButton.UseVisualStyleBackColor = false;
            this.PlayButton.Click += new System.EventHandler(this.PlayButton_Click);
            // 
            // EditButton
            // 
            this.EditButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(21)))), ((int)(((byte)(32)))));
            this.EditButton.FlatAppearance.BorderColor = System.Drawing.Color.DimGray;
            this.EditButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.EditButton.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Bold);
            this.EditButton.ForeColor = System.Drawing.Color.White;
            this.EditButton.Location = new System.Drawing.Point(174, 66);
            this.EditButton.Margin = new System.Windows.Forms.Padding(5);
            this.EditButton.Name = "EditButton";
            this.EditButton.Size = new System.Drawing.Size(74, 41);
            this.EditButton.TabIndex = 6;
            this.EditButton.Text = "编辑";
            this.EditButton.UseVisualStyleBackColor = false;
            this.EditButton.Click += new System.EventHandler(this.EditButton_Click);
            // 
            // AddButton
            // 
            this.AddButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(21)))), ((int)(((byte)(32)))));
            this.AddButton.FlatAppearance.BorderColor = System.Drawing.Color.DimGray;
            this.AddButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.AddButton.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Bold);
            this.AddButton.ForeColor = System.Drawing.Color.White;
            this.AddButton.Location = new System.Drawing.Point(14, 66);
            this.AddButton.Margin = new System.Windows.Forms.Padding(5);
            this.AddButton.Name = "AddButton";
            this.AddButton.Size = new System.Drawing.Size(70, 41);
            this.AddButton.TabIndex = 9;
            this.AddButton.Text = "添加";
            this.AddButton.UseVisualStyleBackColor = false;
            this.AddButton.Click += new System.EventHandler(this.AddButton_Click);
            // 
            // LeftPanel
            // 
            this.LeftPanel.Controls.Add(this.SearchResultPanel);
            this.LeftPanel.Controls.Add(this.SearchPanel);
            this.LeftPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LeftPanel.Location = new System.Drawing.Point(0, 0);
            this.LeftPanel.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.LeftPanel.Name = "LeftPanel";
            this.LeftPanel.Size = new System.Drawing.Size(616, 811);
            this.LeftPanel.TabIndex = 1;
            // 
            // SearchResultPanel
            // 
            this.SearchResultPanel.Controls.Add(this.FileListView);
            this.SearchResultPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SearchResultPanel.Location = new System.Drawing.Point(0, 71);
            this.SearchResultPanel.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.SearchResultPanel.Name = "SearchResultPanel";
            this.SearchResultPanel.Size = new System.Drawing.Size(616, 740);
            this.SearchResultPanel.TabIndex = 1;
            // 
            // FileListView
            // 
            this.FileListView.AllowUserToAddRows = false;
            this.FileListView.AllowUserToResizeColumns = false;
            this.FileListView.AllowUserToResizeRows = false;
            this.FileListView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.FileListView.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCellsExceptHeaders;
            this.FileListView.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(21)))), ((int)(((byte)(32)))));
            this.FileListView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.FileListView.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal;
            this.FileListView.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("微软雅黑", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.FileListView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.FileListView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.FileListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.FileListView.EnableHeadersVisualStyles = false;
            this.FileListView.GridColor = System.Drawing.Color.White;
            this.FileListView.Location = new System.Drawing.Point(0, 0);
            this.FileListView.Name = "FileListView";
            this.FileListView.ReadOnly = true;
            this.FileListView.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.FileListView.RowHeadersVisible = false;
            this.FileListView.RowHeadersWidth = 62;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(30)))), ((int)(((byte)(45)))));
            dataGridViewCellStyle4.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.DimGray;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.FileListView.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this.FileListView.RowTemplate.Height = 27;
            this.FileListView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.FileListView.Size = new System.Drawing.Size(616, 740);
            this.FileListView.TabIndex = 0;
            this.FileListView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.FileListView_CellClick);
            // 
            // SearchPanel
            // 
            this.SearchPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(21)))), ((int)(((byte)(32)))));
            this.SearchPanel.Controls.Add(this.RecordButton);
            this.SearchPanel.Controls.Add(this.SearchBoxPanel);
            this.SearchPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.SearchPanel.Location = new System.Drawing.Point(0, 0);
            this.SearchPanel.Margin = new System.Windows.Forms.Padding(5);
            this.SearchPanel.Name = "SearchPanel";
            this.SearchPanel.Padding = new System.Windows.Forms.Padding(8);
            this.SearchPanel.Size = new System.Drawing.Size(616, 71);
            this.SearchPanel.TabIndex = 0;
            // 
            // RecordButton
            // 
            this.RecordButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(21)))), ((int)(((byte)(32)))));
            this.RecordButton.FlatAppearance.BorderColor = System.Drawing.Color.DimGray;
            this.RecordButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.RecordButton.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Bold);
            this.RecordButton.ForeColor = System.Drawing.Color.White;
            this.RecordButton.Location = new System.Drawing.Point(528, 14);
            this.RecordButton.Margin = new System.Windows.Forms.Padding(5);
            this.RecordButton.Name = "RecordButton";
            this.RecordButton.Size = new System.Drawing.Size(74, 42);
            this.RecordButton.TabIndex = 11;
            this.RecordButton.Text = "录制";
            this.RecordButton.UseVisualStyleBackColor = false;
            this.RecordButton.Click += new System.EventHandler(this.RecordButton_Click);
            // 
            // SearchBoxPanel
            // 
            this.SearchBoxPanel.BackColor = System.Drawing.Color.White;
            this.SearchBoxPanel.Controls.Add(this.SearchBarView);
            this.SearchBoxPanel.Controls.Add(this.SearchIcon);
            this.SearchBoxPanel.Location = new System.Drawing.Point(14, 14);
            this.SearchBoxPanel.Margin = new System.Windows.Forms.Padding(5);
            this.SearchBoxPanel.Name = "SearchBoxPanel";
            this.SearchBoxPanel.Size = new System.Drawing.Size(504, 42);
            this.SearchBoxPanel.TabIndex = 0;
            // 
            // SearchBarView
            // 
            this.SearchBarView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.SearchBarView.Font = new System.Drawing.Font("微软雅黑", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.SearchBarView.Location = new System.Drawing.Point(51, 7);
            this.SearchBarView.Margin = new System.Windows.Forms.Padding(1);
            this.SearchBarView.Name = "SearchBarView";
            this.SearchBarView.Size = new System.Drawing.Size(444, 29);
            this.SearchBarView.TabIndex = 1;
            this.SearchBarView.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.SearchBarView_KeyPress);
            // 
            // SearchIcon
            // 
            this.SearchIcon.Image = global::Vbox.Properties.Resources.search;
            this.SearchIcon.Location = new System.Drawing.Point(0, 0);
            this.SearchIcon.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.SearchIcon.Name = "SearchIcon";
            this.SearchIcon.Size = new System.Drawing.Size(43, 47);
            this.SearchIcon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.SearchIcon.TabIndex = 0;
            this.SearchIcon.TabStop = false;
            // 
            // RepositoryView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(32)))), ((int)(((byte)(39)))));
            this.ClientSize = new System.Drawing.Size(616, 811);
            this.Controls.Add(this.ContainerPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "RepositoryView";
            this.Text = "RepositoryView";
            this.Load += new System.EventHandler(this.RepositoryView_Load);
            this.ContainerPanel.ResumeLayout(false);
            this.DetailPanel.ResumeLayout(false);
            this.DetailPanel.PerformLayout();
            this.LeftPanel.ResumeLayout(false);
            this.SearchResultPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.FileListView)).EndInit();
            this.SearchPanel.ResumeLayout(false);
            this.SearchBoxPanel.ResumeLayout(false);
            this.SearchBoxPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SearchIcon)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel ContainerPanel;
        private System.Windows.Forms.Panel DetailPanel;
        private System.Windows.Forms.Panel LeftPanel;
        private System.Windows.Forms.DataGridView FileListView;
        private System.Windows.Forms.Panel SearchPanel;
        private System.Windows.Forms.Panel SearchBoxPanel;
        private System.Windows.Forms.TextBox SearchBarView;
        private System.Windows.Forms.PictureBox SearchIcon;
        private System.Windows.Forms.Panel SearchResultPanel;
        private System.Windows.Forms.Button DeleteButton;
        private System.Windows.Forms.Button PlayButton;
        private System.Windows.Forms.Label PathDataLabel;
        private System.Windows.Forms.Label PathTitleLabel;
        private System.Windows.Forms.Button ConvertButton;
        private System.Windows.Forms.Button DeleteFromSystemButton;
        private System.Windows.Forms.Button EditButton;
        private System.Windows.Forms.Button AddButton;
        private System.Windows.Forms.Button RecordButton;
    }
}
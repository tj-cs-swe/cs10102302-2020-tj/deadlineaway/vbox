﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vbox
{
    /// <summary>
    /// 仓库的模型类
    /// </summary>
    public class RepositoryModel
    {
        private static List<String> ConfigFiles() {
            var file = File.Open("repository.txt", FileMode.OpenOrCreate);
            var Files = new List<String>();
            using (var stream = new StreamReader(file))
            {
                while (!stream.EndOfStream)
                {
                    Files.Add(stream.ReadLine());
                }
            }
            return Files;
        }

        /// <summary>
        /// 仓库中的文件
        /// </summary>
        private List<FileModel> Files;

        public int FileCount
        {
            get => Files.Count;
        }

        public FileModel FileModelAt(int index)
        {
            return Files[index];
        }

        public RepositoryModel() : this(ConfigFiles()) { }

        /// <summary>
        /// 根据一个列表的路径创建仓库模型
        /// </summary>
        /// <param name="paths">列表里的路径可以是绝对路径或者相对路径</param>
        public RepositoryModel(ICollection<string> paths)
        {
            Files = paths.Select(path => new FileModel(path)).ToList();
        }

        /// <summary>
        /// 给仓库添加文件，而不是文件系统
        /// </summary>
        /// <param name="path">相对路径或者绝对路径</param>
        public void AddFile(String path)
        {
            Files.Add(new FileModel(path));
            UpdateConfigFiles();
        }

        private void UpdateConfigFiles()
        {
            var file = File.Open("repository.txt", FileMode.Truncate);
            using (var stream = new StreamWriter(file)) {
                foreach (var fileModel in Files)
                {
                    stream.WriteLine(fileModel.FullPath);
                }
            }
        }

        /// <summary>
        /// 仅从仓库中删除文件，而不在文件系统中删除
        /// </summary>
        /// <param name="path">要移除文件的相对路径或者绝对路径</param>
        public void RemoveFile(String path)
        {
            FileModel FileToRemove = GetFileModel(path);
            Files.Remove(FileToRemove);
            UpdateConfigFiles();
        }

        public void RemoveFileIndex(int index)
        {
            Files.RemoveAt(index);
            UpdateConfigFiles();
        }

        public void RemoveFileIndexFromRepoAndSystem(int index)
        {
            Files[index].RemoveFromSystem();
            Files.RemoveAt(index);
            UpdateConfigFiles();
        }

        public int SelectedIndex { get; private set; } = -1;

        public FileModel SelectedFile
        {
            get => SelectedIndex == -1 ? null : FileModelAt(SelectedIndex);
        }

        public void SelectFile(String path)
        {
            SelectIndex(GetFileModelIndex(path));
        }

        public void SelectIndex(int index)
        {
            SelectedIndex = index;
        }

        public void UnSelect()
        {
            SelectedIndex = -1;
        }

        /// <summary>
        /// 从仓库和文件系统中删除文件
        /// </summary>
        /// <param name="path"></param>
        public void RemoveFileFromRepoAndSystem(String path)
        {
            FileModel File = GetFileModel(path);
            File.RemoveFromSystem();
            Files.Remove(File);
            UpdateConfigFiles();
        }

        /// <summary>
        /// 辅助方法，返回文件模型类
        /// </summary>
        /// <param name="path">要返回文件的相对路径或者绝对路径</param>
        /// <returns>指向<code>Files</code>里某个路径与<code>path</code>相等的元素的指针</returns>
        public FileModel GetFileModel(String path)
        {
            return Files[GetFileModelIndex(path)];
        }

        /// <summary>
        /// 辅助方法，返回文件模型类的下标
        /// </summary>
        /// <param name="path">要返回文件的相对路径或者绝对路径</param>
        /// <returns>指向<code>Files</code>里某个路径与<code>path</code>相等的元素的下标</returns>
        public int GetFileModelIndex(String path)
        {
            var FullPath = new FileInfo(path).FullName;
            return Files.FindIndex(fileModel => fileModel.FullPath == FullPath);
        }
    }
}

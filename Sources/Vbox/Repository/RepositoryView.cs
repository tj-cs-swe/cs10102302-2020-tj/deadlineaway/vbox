﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using AxTimelineAxLib;

namespace Vbox
{
    public partial class RepositoryView : Form
    {
        private RepositoryModel Repository = new RepositoryModel();

        public RepositoryView()
        {
            InitializeComponent();
        }

        private void UpdateFileListFromModel(String FilterKeyword = "")
        {
            var DataTable = new DataTable("Files");
            DataTable.Columns.Add("文件名");
            DataTable.Columns.Add("修改时间");
            DataTable.Columns.Add("大小");

            for (int i = 0; i < Repository.FileCount; ++i) {
                var FileModel = Repository.FileModelAt(i);
                if (FileModel.Name.ToLower().Contains(FilterKeyword.ToLower()))
                {
                    DataTable.Rows.Add(FileModel.Name, FileModel.LastWriteTime, $"{FileModel.SizeKB} KB");
                }
            }
            FileListView.DataSource = DataTable;
        }

        private void UpdateSelectedFileViewFromModel()
        {
            var SelectedFile = Repository.SelectedFile;
            if (SelectedFile == null)
            {
                PathDataLabel.Text = "点击列表选择视频";
            }
            else
            {
                PathDataLabel.Text = SelectedFile.FullPath;
            }
        }

        private void RepositoryView_Load(object sender, EventArgs e)
        {
            UpdateFileListFromModel();
            UpdateSelectedFileViewFromModel();
        }

        private void SearchBarView_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                string Keyword = SearchBarView.Text.Trim();
                UpdateFileListFromModel(Keyword);
            }
        }

        private void FileListView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (FileListView.SelectedRows.Count == 0) { return; }
            Repository.SelectIndex(e.RowIndex);
            UpdateSelectedFileViewFromModel();
        }

        private void DeleteButton_Click(object sender, EventArgs e)
        {
            if (Repository.SelectedFile == null) { return; }

            var FileName = Repository.SelectedFile.Name;
            DialogResult result = MessageBox.Show($"确认将'{FileName}'从仓库删除？", "操作提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
            if (result == DialogResult.OK)
            {
                Repository.RemoveFileIndex(Repository.SelectedIndex);
                Repository.UnSelect();
                MessageBox.Show("删除成功!", "操作提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                UpdateFileListFromModel();
                UpdateSelectedFileViewFromModel();
            }
        }

        private void PlayButton_Click(object sender, EventArgs e)
        {
            if (FileSelectedNotSafe()) { return; }

            if (Repository.SelectedFile == null) { return; }

            new VideoPlayerView(new VideoPlayerModel(Repository.SelectedFile)).ShowDialog();
        }


        private void DeleteFromSystemButton_Click(object sender, EventArgs e)
        {
            if (FileSelectedNotSafe()) { return; }

            var FileName = Repository.SelectedFile.Name;
            DialogResult result = MessageBox.Show($"确认将'{FileName}'从仓库和系统中删除？", "操作提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
            if (result == DialogResult.OK)
            {
                Repository.RemoveFileIndexFromRepoAndSystem(Repository.SelectedIndex);
                Repository.UnSelect();
                MessageBox.Show("删除成功!", "操作提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                UpdateFileListFromModel();
                UpdateSelectedFileViewFromModel();
            }
        }

        private bool FileSelectedNotSafe()
        {
            if (Repository.SelectedFile == null) { return true; }

            if (!Repository.SelectedFile.Exists)
            {
                PromptToDeleteFromRepo();
                return true;
            }

            return false;
        }

        private void PromptToDeleteFromRepo()
        {
            DialogResult resultWhenFileNotExists = MessageBox.Show($"文件'{Repository.SelectedFile.Name}'已经不存在系统中，是否将其从仓库中删除?", "操作提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
            if (resultWhenFileNotExists == DialogResult.OK)
            {
                Repository.RemoveFileIndex(Repository.SelectedIndex);
                Repository.UnSelect();
                UpdateFileListFromModel();
                UpdateSelectedFileViewFromModel();
            }
        }

        private void AddButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "(video files)|*.mp4;*.avi;*.mov;";
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                Repository.AddFile(openFileDialog.FileName);
                UpdateFileListFromModel();
            }
        }

        private void ConvertButton_Click(object sender, EventArgs e)
        {
            if (FileSelectedNotSafe()) { return; }

            var Dialog = new SaveFileDialog();
            Dialog.Filter = "MP4 Files | *.mp4 |MOV Files| *.mov |AVI Files | *.avi";
            if (Dialog.ShowDialog() == DialogResult.OK)
            {
                if (File.Exists(Dialog.FileName))
                {
                    File.Delete(Dialog.FileName);
                }
                File.Copy(Repository.SelectedFile.FullPath, Dialog.FileName);
                Repository.AddFile(Dialog.FileName);
                UpdateFileListFromModel();
            }
        }

        private void EditButton_Click(object sender, EventArgs e)
        {
            if (FileSelectedNotSafe()) { return; }

            new EditorView(Repository).ShowDialog();
            UpdateFileListFromModel();
        }

        private void RecordButton_Click(object sender, EventArgs e)
        {
            this.Hide();
            new VideoRecorder(Repository).ShowDialog();
            this.Show();
            UpdateFileListFromModel();
        }
    }
}

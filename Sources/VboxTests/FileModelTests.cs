﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Vbox;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.IO;

namespace Vbox.Tests
{
    [TestClass()]
    public class FileModelTests
    {
        [TestMethod()]
        public void FileModelTest()
        {
            var Model = new FileModel("..\\..\\TestFiles\\TestVideo.mp4");
            CollectionAssert.AllItemsAreNotNull(new object[] {
                Model,
                Model.FullPath,
                Model.Name,
                Model.SizeKB,
            });
        }

        [TestMethod()]
        public void PropertyTest()
        {
            var Model = new FileModel("..\\..\\TestFiles\\TestVideo.mp4");
            Assert.AreEqual("TestVideo.mp4", Model.Name);
            Assert.AreEqual(257, Model.SizeKB);
        }

        [TestMethod()]
        public void RemoveFromSystemTest()
        {
            var Path = "..\\..\\TestFiles\\Test.txt";

            if (File.Exists(Path))
            {
                File.Delete(Path);
            }
            Assert.IsTrue(!File.Exists(Path));

            File.Create(Path).Close();
            Assert.IsTrue(File.Exists(Path));

            var Model = new FileModel(Path);
            Model.RemoveFromSystem();
            Assert.IsTrue(!File.Exists(Path));
        }
    }
}
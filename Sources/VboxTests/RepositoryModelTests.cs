﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Vbox;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Vbox.Tests
{
    [TestClass()]
    public class RepositoryModelTests
    {
        private const string TestPath = "..\\..\\TestFiles\\Test.txt";

        [TestMethod()]
        public void RepositoryModelTest()
        {
            var Model = new RepositoryModel();
            Assert.AreEqual(0, Model.FileCount);

            Model = NewModel();
            Assert.AreEqual(5, Model.FileCount);
        }

        [TestMethod()]
        public void FileModelAtTest()
        {
            var Model = NewModel();
            var FileModel = Model.FileModelAt(2);
            Assert.AreEqual("TestVideo.mp4", FileModel.Name);
            Assert.AreEqual(257, FileModel.SizeKB);
        }

        [TestMethod()]
        public void AddFileTest()
        {
            CreateTestFile();

            var Model = NewModel();
            Model.AddFile(TestPath);
            Assert.AreEqual(6, Model.FileCount);
            Assert.AreEqual("Test.txt", Model.FileModelAt(5).Name);

            CloseTestFile();
        }

        [TestMethod()]
        public void RemoveFileTest()
        {
            CreateTestFile();

            var Model = NewModel();
            Model.AddFile(TestPath);
            Model.RemoveFile(TestPath);

            Assert.IsTrue(File.Exists(TestPath));
            Assert.AreEqual(5, Model.FileCount);

            CloseTestFile();
        }

        [TestMethod()]
        public void RemoveFileFromRepoAndSystemTest()
        {
            CreateTestFile();

            var Model = NewModel();
            Model.AddFile(TestPath);
            Model.RemoveFileFromRepoAndSystem(TestPath);

            Assert.IsTrue(!File.Exists(TestPath));
            Assert.AreEqual(5, Model.FileCount);

            CloseTestFile();
        }

        private static RepositoryModel NewModel()
        {
            return new RepositoryModel(new string[]{
                "..\\..\\TestFiles\\Test1.txt",
                "..\\..\\TestFiles\\Test2.txt",
                "..\\..\\TestFiles\\TestVideo.mp4",
                "..\\..\\TestFiles\\Test3.txt",
                "..\\..\\TestFiles\\Test4.txt",
            });
        }

        private static void CreateTestFile()
        {
            if (File.Exists(TestPath))
            {
                CloseTestFile();
            }
            File.Create(TestPath).Close();
        }

        private static void CloseTestFile()
        {
            File.Delete(TestPath);
        }
    }
}
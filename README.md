# Vbox

Vbox 是一款集成录制、播放、编辑功能为一体的软件。

## 源代码文档

我们采用 Doxygen 将代码注释文档化，可以访问[该网址](https://cs10102302-2020-tj.gitlab.io/deadlineaway/vbox/index.html)。

## 产品简介

整个产品的起始界面是仓库界面，通过仓库界面可以选择视频，进入以下三个界面：

- 播放
- 编辑
- 录制

录制、编辑完成后，视频均会保存到仓库。

## License

该产品的最终解释权归同济大学 Deadline Away 开发团队所有，不允许在未经许可的情况下擅自商用。